package com.kinetica.query;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpudb.GPUdb;
import com.gpudb.Record;
import com.kinetica.query.domain.BookCurrencyPair;
import com.kinetica.query.domain.QueryObject;
import com.kinetica.query.domain.output.PNL;
import com.kinetica.util.KineticaConfiguration;

public class QuantManager {

	public final static Logger log = 
			LoggerFactory.getLogger(QuantManager.class);

	public List<PNL> determinePNLList(String transactionId, 
			List<BookCurrencyPair> bookCurrencyPair) {
		
		QueryEngine engine = new QueryEngine();
 		QueryObject queryObject = new QueryObject();
 		queryObject.setTableName(
 			KineticaConfiguration.
 				FX_QUANT_LIB_TABLE_NAME);
 		queryObject.setCollectionName(KineticaConfiguration.COLLECTION_NAME);
 		queryObject.setLimit(GPUdb.END_OF_SET);
 
 		List<String> columnNames = new ArrayList<String>();
 		columnNames.add("book");
 		columnNames.add("currency_pair");
 		columnNames.add("sum(trade_pv)");
 		columnNames.add("sum(trade_base_pv)");
 		columnNames.add("sum(trade_term_delta)");
 		columnNames.add("sum(trade_base_delta)");
 		columnNames.add("sum(trade_usd_term_delta)");
 		columnNames.add("sum(trade_base_gamma)");
 		columnNames.add("sum(trade_theta)");
 		
 		queryObject.setColumnNames(columnNames);

		List<PNL> returnList = new ArrayList<PNL>();

 		List<Record> records = engine.aggregateGroupBy(queryObject);
 		for (Record record : records) {
 			String book = record.getString("book");
 			String currencyPair = record.getString("currency_pair");
 			BookCurrencyPair bcPair = new BookCurrencyPair(book, currencyPair);
 			if (bookCurrencyPair.contains(bcPair)) {
 	 			PNL pnl = new PNL();
 				pnl.transactionId = transactionId;
 				pnl.book = book;
 				pnl.currencyPair = currencyPair;
 				pnl.trade_pv = new Double(record.getString("sum(trade_pv)"));
 				pnl.trade_base_pv = new Double(record.getString("sum(trade_base_pv)"));
 				pnl.trade_term_delta = new Double(record.getString("sum(trade_term_delta)"));
 				pnl.trade_base_delta = new Double(record.getString("sum(trade_base_delta)"));
 				pnl.trade_usd_term_delta = new Double(record.getString("sum(trade_usd_term_delta)"));
 				pnl.trade_base_gamma = new Double(record.getString("sum(trade_base_gamma)"));
 				pnl.trade_theta = new Double(record.getString("sum(trade_theta)"));
 
 				pnl.updated = Calendar.getInstance().getTimeInMillis();
 				returnList.add(pnl);
 			}
		}
		return returnList;
	}
}

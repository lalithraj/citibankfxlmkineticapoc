package com.kinetica.query.domain.output;

import com.gpudb.RecordObject;

public class PNL_JSON extends RecordObject {

	@RecordObject.Column(order = 0, properties = { "data", "char32" })
	public String transactionId;

	@RecordObject.Column(order = 1, properties = { "data" })
	public String pnl_json;
}

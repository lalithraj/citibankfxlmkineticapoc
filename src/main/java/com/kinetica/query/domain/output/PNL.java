package com.kinetica.query.domain.output;

import com.gpudb.RecordObject;

public class PNL extends RecordObject {

	@RecordObject.Column(order = 0, properties = { "data", "char32" })
	public String transactionId;

	@RecordObject.Column(order = 1, properties = { "data", "char4" })
	public String book;

	@RecordObject.Column(order = 2, properties = { "data", "char8" })
	public String currencyPair;

	@RecordObject.Column(order = 3, properties = { "data", "timestamp" })
	public long updated;

	@RecordObject.Column(order = 4, properties = { "data" })
	public Double trade_pv;

	@RecordObject.Column(order = 5, properties = { "data" })
	public Double trade_base_pv;

	@RecordObject.Column(order = 6, properties = { "data" })
	public Double trade_term_delta;

	@RecordObject.Column(order = 7, properties = { "data" })
	public Double trade_base_delta;

	@RecordObject.Column(order = 8, properties = { "data" })
	public Double trade_usd_term_delta;

	@RecordObject.Column(order = 9, properties = { "data" })
	public Double trade_base_gamma;

	@RecordObject.Column(order = 10, properties = { "data" })
	public Double trade_theta;
}

package com.kinetica.query.domain.output;

public class PNL_REPORT {

	public String transactionId;
	public String book;
	public String currencyPair;
	public long updated;
	public Double trade_pv;
	public Double trade_base_pv;
	public Double trade_term_delta;
	public Double trade_base_delta;
	public Double trade_usd_term_delta;
	public Double trade_base_gamma;
	public Double trade_theta;
}

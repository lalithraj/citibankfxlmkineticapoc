package com.kinetica.query.domain;

import com.gpudb.RecordObject;

public class BookCurrencyPair extends RecordObject {

	@RecordObject.Column(order = 0, properties = { "data", "primary_key" })
	public int id;
	
	@RecordObject.Column(order = 1, properties = { "data", "char16" })
	public String book;

	@RecordObject.Column(order = 2, properties = { "data", "char16" })
	public String currency;

	public BookCurrencyPair() {
	}

	public BookCurrencyPair(String book, String currency) {
		super();
		this.book = book;
		this.currency = currency;
	}

	@Override
	public boolean equals(Object bookCurrencyPairObj) {
		BookCurrencyPair bcPair = (BookCurrencyPair) bookCurrencyPairObj;
		if (bcPair.book.equalsIgnoreCase(this.book) && 
			bcPair.currency.equalsIgnoreCase(this.currency)) {
				return true;
		}
		return false;
	}
}

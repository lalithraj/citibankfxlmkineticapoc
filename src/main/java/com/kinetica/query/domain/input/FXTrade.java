package com.kinetica.query.domain.input;

public class FXTrade {

	private int traderId;
	private long marketDataTime;
	private long calcTime;
	private int noOfValuationOutputs;
	private String resultType;
	private String triggeringEventDescription;
	private String producedByTopology;
	private String book;
	private long tradeId;
	private int tradeVersion;
	private String acceptStatus;
	private String description;
	private long foxId;
	private boolean isEODTrade;
	private boolean isIntraDayTrade;
	private boolean isForwardPremiumTrade;
	private boolean isActiveTrade;
	private long productId;
	private String actionStatus;
	private String tradeType;
	private long strikePrice;
	private long notional;
	private String currencyPair;
	private int updateType;
	private int riskGroup;
	private int riskItem;
	private int fxoOptionCode;
	private long premium;

	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	public long getStrikePrice() {
		return strikePrice;
	}
	public void setStrikePrice(long strikePrice) {
		this.strikePrice = strikePrice;
	}
	public long getNotional() {
		return notional;
	}
	public void setNotional(long notional) {
		this.notional = notional;
	}
	public String getCurrencyPair() {
		return currencyPair;
	}
	public void setCurrencyPair(String currencyPair) {
		this.currencyPair = currencyPair;
	}
	public int getUpdateType() {
		return updateType;
	}
	public void setUpdateType(int updateType) {
		this.updateType = updateType;
	}
	public int getRiskGroup() {
		return riskGroup;
	}
	public void setRiskGroup(int riskGroup) {
		this.riskGroup = riskGroup;
	}
	public int getRiskItem() {
		return riskItem;
	}
	public void setRiskItem(int riskItem) {
		this.riskItem = riskItem;
	}
	public int getFxoOptionCode() {
		return fxoOptionCode;
	}
	public void setFxoOptionCode(int fxoOptionCode) {
		this.fxoOptionCode = fxoOptionCode;
	}
	public long getPremium() {
		return premium;
	}
	public void setPremium(long premium) {
		this.premium = premium;
	}
	public int getTraderId() {
		return traderId;
	}
	public void setTraderId(int traderId) {
		this.traderId = traderId;
	}
	public long getMarketDataTime() {
		return marketDataTime;
	}
	public void setMarketDataTime(long marketDataTime) {
		this.marketDataTime = marketDataTime;
	}
	public long getCalcTime() {
		return calcTime;
	}
	public void setCalcTime(long calcTime) {
		this.calcTime = calcTime;
	}
	public int getNoOfValuationOutputs() {
		return noOfValuationOutputs;
	}
	public void setNoOfValuationOutputs(int noOfValuationOutputs) {
		this.noOfValuationOutputs = noOfValuationOutputs;
	}
	public String getResultType() {
		return resultType;
	}
	public void setResultType(String resultType) {
		this.resultType = resultType;
	}
	public String getTriggeringEventDescription() {
		return triggeringEventDescription;
	}
	public void setTriggeringEventDescription(String triggeringEventDescription) {
		this.triggeringEventDescription = triggeringEventDescription;
	}
	public String getProducedByTopology() {
		return producedByTopology;
	}
	public void setProducedByTopology(String producedByTopology) {
		this.producedByTopology = producedByTopology;
	}
	public String getBook() {
		return book;
	}
	public void setBook(String book) {
		this.book = book;
	}
	public long getTradeId() {
		return tradeId;
	}
	public void setTradeId(long tradeId) {
		this.tradeId = tradeId;
	}
	public int getTradeVersion() {
		return tradeVersion;
	}
	public void setTradeVersion(int tradeVersion) {
		this.tradeVersion = tradeVersion;
	}
	public String getAcceptStatus() {
		return acceptStatus;
	}
	public void setAcceptStatus(String acceptStatus) {
		this.acceptStatus = acceptStatus;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getFoxId() {
		return foxId;
	}
	public void setFoxId(long foxId) {
		this.foxId = foxId;
	}
	public boolean isEODTrade() {
		return isEODTrade;
	}
	public void setEODTrade(boolean isEODTrade) {
		this.isEODTrade = isEODTrade;
	}
	public boolean isIntraDayTrade() {
		return isIntraDayTrade;
	}
	public void setIntraDayTrade(boolean isIntraDayTrade) {
		this.isIntraDayTrade = isIntraDayTrade;
	}
	public boolean isForwardPremiumTrade() {
		return isForwardPremiumTrade;
	}
	public void setForwardPremiumTrade(boolean isForwardPremiumTrade) {
		this.isForwardPremiumTrade = isForwardPremiumTrade;
	}
	public boolean isActiveTrade() {
		return isActiveTrade;
	}
	public void setActiveTrade(boolean isActiveTrade) {
		this.isActiveTrade = isActiveTrade;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public String getActionStatus() {
		return actionStatus;
	}
	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}
}

package com.kinetica.query.domain;

import java.util.List;
import java.util.Map;

public class QueryObject {

	private String collectionName;
	private String tableName;
	private String viewName;
	private long limit;
	private long offset;
	private String expression = "";
	private List<String> columnNames;
	private Map<String, String> options;

	public QueryObject(String tableName, 
			String viewName, long limit, 
				long offset, String expression) {
		super();
		this.tableName = tableName;
		this.viewName = viewName;
		this.limit = limit;
		this.offset = offset;
		this.expression = expression;
	}

	public QueryObject(String tableName, 
			String viewName, long limit, 
				long offset, List<String> columnNames) {
		super();
		this.tableName = tableName;
		this.viewName = viewName;
		this.limit = limit;
		this.offset = offset;
		this.columnNames = columnNames;
	}	

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public QueryObject() {
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public long getLimit() {
		return limit;
	}

	public void setLimit(long limit) {
		this.limit = limit;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}



	public List<String> getColumnNames() {
		return columnNames;
	}



	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}

	public Map<String, String> getOptions() {
		return options;
	}

	public void setOptions(Map<String, String> options) {
		this.options = options;
	}
}
package com.kinetica.query;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.gpudb.BulkInserter;
import com.gpudb.GPUdb;
import com.gpudb.GPUdbException;
import com.gpudb.Record;
import com.kinetica.query.domain.BookCurrencyPair;
import com.kinetica.query.domain.QueryObject;
import com.kinetica.query.domain.output.PNL;
import com.kinetica.query.domain.output.PNL_JSON;
import com.kinetica.query.domain.output.PNL_REPORT;
import com.kinetica.util.DateTimeUtils;
import com.kinetica.util.GPUConnectionManager;
import com.kinetica.util.KineticaConfiguration;
import com.kinetica.util.KineticaException;
import com.kinetica.util.TypeUtils;
import com.kinetica.util.jdbc.JDBCEngine;

public class FXTradeManager {

	public final static Logger log = 
		LoggerFactory.getLogger(FXTradeManager.class);

	public List<PNL> getPNLListUsingJDBCQuery() {
 		TransactionGenerator tg = new TransactionGenerator();
 		String transactionID = tg.generateTransactionID();

		JDBCEngine engine = JDBCEngine.getInstance();
		try {
			engine.init();
			ResultSet resultSet = engine.execute(
				KineticaConfiguration.QUERY_STRING);
			
			List<PNL> pnlList = new ArrayList<PNL>();
			
			while(resultSet.next()) {
				PNL pnl = new PNL();
				pnl.book = resultSet.getString(1);
				pnl.currencyPair = resultSet.getString(2);
				pnl.trade_pv = resultSet.getDouble(3);
				pnl.trade_base_pv = resultSet.getDouble(4);
				pnl.trade_term_delta = resultSet.getDouble(5);
				pnl.trade_base_delta = resultSet.getDouble(6);
				pnl.trade_usd_term_delta = resultSet.getDouble(7);
				pnl.trade_base_gamma = resultSet.getDouble(8);
				pnl.trade_theta = resultSet.getDouble(9);

				pnl.updated = DateTimeUtils.getTodaysTimeMillis();
				pnl.transactionId = transactionID;
				pnlList.add(pnl);
			}
			return pnlList;
		} catch (Exception e) {
			throw new KineticaException(e);
		} finally {
			engine.close();
		}
	}
	
	public List<BookCurrencyPair> determineFXTradesGroupByBookCurrencyPair() {
		QueryEngine engine = new QueryEngine();
 		QueryObject queryObject = new QueryObject();
 		queryObject.setTableName(
 			KineticaConfiguration.
 				FX_TRADE_TABLE_NAME);
 		queryObject.setLimit(GPUdb.END_OF_SET);
 		
 		List<String> columnNames = new ArrayList<String>();
 		columnNames.add("book");
 		columnNames.add("currency_pair");

 		queryObject.setColumnNames(columnNames);
 		
 		Map<String, String> options = new HashMap<String, String>();
 		options.put("collection_name", KineticaConfiguration.COLLECTION_NAME);
 		options.put("expression", "isLiveTrade = 'true'");
 		
 		List<Record> records = engine.aggregateGroupBy(queryObject);

 		if (records.size() == 0) {
 			throw new KineticaException("No data found for query.");
 		}
 		List<BookCurrencyPair> returnList = new ArrayList<BookCurrencyPair>();
 		int count = 0;
 		for (Record record : records) {
 			count++;
 			//For every Book/Currency, add a record in the table
 			BookCurrencyPair bcPair = new BookCurrencyPair();
 			bcPair.id = count;
 			bcPair.book = record.getString(0);
 			bcPair.currency = record.getString(1);
 			returnList.add(bcPair);
		}
 		return returnList;
	}

	public void persistbcPairs(List<BookCurrencyPair> bcPairs) {
 		BulkInserter<BookCurrencyPair> bulkInserter;
 		try {
 			//For every Book/Currency, add a record in the table
			bulkInserter = new BulkInserter<BookCurrencyPair>(GPUConnectionManager.
				getInstance().getGPUdb(), "BOOK_CURRENCY_PAIR", 
					TypeUtils.getType(BookCurrencyPair.class), 10000, null);
			bulkInserter.insert(bcPairs);
			bulkInserter.flush();
		} catch (GPUdbException e) {
			throw new KineticaException(e);
		}
	}

	public List<PNL> getPNLList(List<BookCurrencyPair> bookCurrencyPair) {
 
 		TransactionGenerator tg = new TransactionGenerator();
 		String transactionID = tg.generateTransactionID();
 		QuantManager qm = new QuantManager();
		return qm.determinePNLList(transactionID, bookCurrencyPair);
	}

	public void persistPNLList(List<PNL> pnlList) {		
 		BulkInserter<PNL> bulkInserterPNL;
 		try {

 			bulkInserterPNL = new BulkInserter<PNL>(GPUConnectionManager.
 				getInstance().getGPUdb(), "PNL", TypeUtils.getType(PNL.class), 10000, null);
			bulkInserterPNL.insert(pnlList);
			bulkInserterPNL.flush();
			log.info("Completed generating PNL.");
 		} catch (Exception e) {
 			throw new KineticaException(e);
 		}
	}

	public void persistPNLListAsJSON(List<PNL> pnlList) {
		List<PNL_JSON> pnlJSONList = getPNLJSONList(pnlList);
		
 		BulkInserter<PNL_JSON> bulkInserterPNL_JSON;
 		try {
 			bulkInserterPNL_JSON = new BulkInserter<PNL_JSON>(GPUConnectionManager.
 				getInstance().getGPUdb(), "PNL_JSON", TypeUtils.getType(PNL_JSON.class), 10000, null);
			bulkInserterPNL_JSON.insert(pnlJSONList);
			bulkInserterPNL_JSON.flush();
			log.info("Completed generating PNL_JSON.");
 		} catch (Exception e) {
 			throw new KineticaException(e);
 		}
	}

	private List<PNL_JSON> getPNLJSONList(List<PNL> pnlList) {
		List<PNL_REPORT> pnlReportList = getPNL_REPORT(pnlList);
		
		List<PNL_JSON> pnlJSONList = new ArrayList<PNL_JSON>();
		Gson gson = new Gson();
		for (PNL_REPORT pnlReport : pnlReportList) {
			PNL_JSON pnlJSON = new PNL_JSON();
			pnlJSON.transactionId = pnlReport.transactionId;
			pnlJSON.pnl_json = gson.toJson(pnlReport);
			
			pnlJSONList.add(pnlJSON);
		}
		return pnlJSONList;
	}

	private List<PNL_REPORT> getPNL_REPORT(List<PNL> pnlList) {
		List<PNL_REPORT> pnlReportList = new ArrayList<PNL_REPORT>();
		for (PNL pnl : pnlList) {
			PNL_REPORT pnlReport = new PNL_REPORT();
			pnlReport.transactionId = pnl.transactionId;
			pnlReport.book = pnl.book;
			pnlReport.currencyPair = pnl.currencyPair;
			pnlReport.updated = pnl.updated;
			pnlReport.trade_pv = pnl.trade_pv;
			pnlReport.trade_base_pv = pnl.trade_base_pv;
			pnlReport.trade_term_delta = pnl.trade_term_delta;
			pnlReport.trade_base_delta = pnl.trade_base_delta;
			pnlReport.trade_usd_term_delta = pnl.trade_usd_term_delta;
			pnlReport.trade_base_gamma = pnl.trade_base_gamma;
			pnlReport.trade_theta = pnl.trade_theta;
			
			pnlReportList.add(pnlReport);
		}
		return pnlReportList;
	}
}
package com.kinetica.query;

import com.kinetica.util.RandomUtil;

public class TransactionGenerator {

	public String generateTransactionID() {
		return "TR" + RandomUtil.generateRandomNumeric(10);
	}
}
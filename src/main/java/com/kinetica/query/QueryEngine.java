package com.kinetica.query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpudb.GPUdb;
import com.gpudb.Record;
import com.gpudb.protocol.AggregateGroupByRequest;
import com.gpudb.protocol.GetRecordsRequest;
import com.gpudb.protocol.GetRecordsResponse;
import com.kinetica.query.domain.QueryObject;
import com.kinetica.util.GPUConnectionManager;
import com.kinetica.util.KineticaException;

public class QueryEngine {

	private final static Logger log = 
		LoggerFactory.getLogger(QueryEngine.class);
	private GPUdb gpudb = null;

	public QueryEngine() {
		this.gpudb = GPUConnectionManager.
			getInstance().getGPUdb();
	}

	public List<Record> aggregateGroupBy(QueryObject queryObject) {
		try {
			List<Record> returnList = aggrGroupBy(queryObject);
			log.info("Executed BookCurrency aggregate groupby query.");
			return returnList;
		} catch (KineticaException e) {
			throw e;
		}
	}
	
	public <T> List<T> queryByExpression(QueryObject queryObject) {
		String uniqueID = UUID.randomUUID().toString();
		String viewName = queryObject.getTableName() + "_" + uniqueID;
		queryObject.setViewName(viewName);
		log.debug("Auto generated ViewName is: " + viewName);
		try {
			long startTime = System.currentTimeMillis();
			List<T> returnList = queryByExpr(queryObject);
			long endTime = System.currentTimeMillis();
			log.debug("QueryTime milliseconds: " + (endTime - startTime));
			return returnList;
		} catch (KineticaException e) {
			throw e;
		} finally {
			dropTableOrView(viewName);
		}
	}

	private <T> List<T> queryByExpr(QueryObject queryObject) {
		try {
			gpudb.filter(queryObject.getTableName(), 
				queryObject.getViewName(), queryObject.getExpression(), null);
			GetRecordsRequest request = new GetRecordsRequest();
			request.setTableName(queryObject.getViewName());
			request.setLimit(queryObject.getLimit());
			request.setOffset(queryObject.getOffset());
			GetRecordsResponse<T> getRecordResponse = gpudb.getRecords(request);
	
			return getRecordResponse.getData();				
		} catch (Exception e) {
			throw new KineticaException("Could not queryByExpression:", e);
		}
	}
	
	private List<Record> aggrGroupBy(QueryObject queryObject) {
		try {
			AggregateGroupByRequest aggregateGroupByRequest 
				= getAggregateGroupByRequest(queryObject);
			return gpudb.aggregateGroupBy(
				aggregateGroupByRequest).getData();
		} catch (Exception e) {
			throw new KineticaException("Error in aggrGroupBy:", e);
		}
	}
	
	private AggregateGroupByRequest getAggregateGroupByRequest(QueryObject queryObject) {
		AggregateGroupByRequest request = new AggregateGroupByRequest();
		request.setTableName(queryObject.getTableName());
		request.setLimit(queryObject.getLimit());
		request.setOffset(queryObject.getOffset());
		request.setColumnNames(queryObject.getColumnNames());
		
		Map<String, String> options = new HashMap<String, String>();
		options.put("collection_name", queryObject.getCollectionName());
		options.put("expression", queryObject.getExpression());
		request.setOptions(queryObject.getOptions());
		return request;
	}

	public void dropTableOrView(String name) {
		try {
			if (gpudb.hasTable(name, null).getTableExists()) {
				gpudb.clearTable(name, "", null);	
			} else {
				log.warn("Unable to drop Table or View. " + name + " doesnt exist.");
			}
		} catch (Exception e) {
			throw new KineticaException("Could not dropTableOrView:", e);
		}
	}

	public <T> List<T> getRecords(QueryObject queryObject) {
		try {
			GetRecordsRequest request = new GetRecordsRequest();
			request.setTableName(queryObject.getTableName());
			request.setLimit(queryObject.getLimit());
			request.setOffset(queryObject.getOffset());
			GetRecordsResponse<T> getRecordResponse = gpudb.getRecords(request);
			return getRecordResponse.getData();
		} catch (Exception e) {
			throw new KineticaException("Error in getRecords:", e);
		}

	}
}
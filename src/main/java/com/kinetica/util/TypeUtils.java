package com.kinetica.util;

import com.gpudb.GPUdb;
import com.gpudb.GPUdbException;
import com.gpudb.RecordObject;
import com.gpudb.Type;

public class TypeUtils {

	public static String getTypeId(Type type) {
		try {
			GPUdb gpuDB = GPUConnectionManager.getInstance().getGPUdb();
			return type.create(gpuDB);
		} catch (GPUdbException e) {
			throw new KineticaException(e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Type getType(Class _class) {
		return RecordObject.getType(_class);
	}
}
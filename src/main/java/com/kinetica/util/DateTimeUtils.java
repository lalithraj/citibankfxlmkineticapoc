package com.kinetica.util;

import java.util.Calendar;
import java.util.Date;

public class DateTimeUtils {

	public static long getTodaysTimeMillis() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return cal.getTimeInMillis();
	}
}
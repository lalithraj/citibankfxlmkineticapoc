package com.kinetica.util;

import java.util.Properties;

public abstract class KineticaConfiguration {

	private static Properties kineticaProperties = 
			KineticaPropertyReader.getInstance();

	public static final String GPUDB_CONNECT_STRING = 
		kineticaProperties.getProperty("GPUDB.CONNECT_STRING");
	
	public static final String ENABLE_AUTHENTICATION = 
		kineticaProperties.getProperty("KINETICA.ENABLE_AUTHENTICATION");

	public static final String USERNAME = 
		kineticaProperties.getProperty("KINETICA.USERNAME");

	public static final String PASSWORD = 
		kineticaProperties.getProperty("KINETICA.PASSWORD");

	public static final String COLLECTION_NAME = 
		kineticaProperties.getProperty("KINETICA.COLLECTION_NAME");
	
	public static final String FX_TRADE_TABLE_NAME = 
		kineticaProperties.getProperty("KINETICA.FX_TRADE_TABLE_NAME");

	public static final String FX_QUANT_LIB_TABLE_NAME = 
		kineticaProperties.getProperty("KINETICA.FX_QUANT_LIB_TABLE_NAME");

	public static final String QUERY_STRING = 
		kineticaProperties.getProperty("KINETICA.QUERY_STRING");

	public static final String JDBC_CONNECTION_URL = 
		kineticaProperties.getProperty("KINETICA.JDBC_CONNECTION_URL");

	public static final String JDBC_OR_GPUDBFILTER = 
		kineticaProperties.getProperty("KINETICA.JDBC_OR_GPUDBFILTER");

}
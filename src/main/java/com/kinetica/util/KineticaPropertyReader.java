package com.kinetica.util;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KineticaPropertyReader {

	private final static Logger logger = 
		LoggerFactory.getLogger(KineticaPropertyReader.class);

	private static Properties properties = null;

	//Private Constructor to avoid initiation
	private KineticaPropertyReader() {
    }

	public static Properties getInstance() {
		if (properties == null) {
			throw new KineticaException("Please initialize the property file.");
		}
		return properties;
	}

	public static void init(String propertiesFile) {
		FileInputStream inputStream = null;
		try {
			logger.info("Loading properties from file: " + propertiesFile);
			inputStream = new FileInputStream(propertiesFile);
			properties = new Properties();
			properties.load(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
			throw new KineticaException(e);
		}
		finally {
			try {
				inputStream.close();
			} catch (Exception e) {
				throw new KineticaException(e);
			}
		}
	}

	public static void validateProperties() {
		if (StringUtils.isEmpty(KineticaConfiguration.GPUDB_CONNECT_STRING)) {
			throw new KineticaException("Property GPUDB.CONNECT_STRING can't be null");
		}
		if (StringUtils.isEmpty(KineticaConfiguration.COLLECTION_NAME)) {
			throw new KineticaException("Property KINETICA.COLLECTION_NAME can't be null");
		}
		if (StringUtils.isEmpty(KineticaConfiguration.FX_TRADE_TABLE_NAME)) {
			throw new KineticaException("Property KINETICA.FX_TRADE_TABLE_NAME can't be null");
		}
		if (StringUtils.isEmpty(KineticaConfiguration.FX_QUANT_LIB_TABLE_NAME)) {
			throw new KineticaException("Property KINETICA.FX_QUANT_LIB_TABLE_NAME can't be null");
		}
		if (StringUtils.isEmpty(KineticaConfiguration.JDBC_OR_GPUDBFILTER)) {
			throw new KineticaException("Property KINETICA.JDBC_OR_GPUDBFILTER can't be null");
		}

	}
}
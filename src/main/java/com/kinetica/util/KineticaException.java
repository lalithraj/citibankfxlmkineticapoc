package com.kinetica.util;

public class KineticaException extends RuntimeException {

	private static final long serialVersionUID = 3064406863292282746L;

	public KineticaException() {
		super();
	}

	public KineticaException(String message, Throwable cause) {
		super(message, cause);
	}

	public KineticaException(String message) {
		super(message);
	}

	public KineticaException(Throwable cause) {
		super(cause);
	}	
}
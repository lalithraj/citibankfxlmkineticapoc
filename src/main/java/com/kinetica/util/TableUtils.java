package com.kinetica.util;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpudb.GPUdb;
import com.gpudb.GPUdbException;
import com.gpudb.protocol.CreateTableRequest;
import com.gpudb.protocol.HasTableRequest;
import com.kinetica.query.domain.Table;

public class TableUtils {

	private final static Logger logger = 
		LoggerFactory.getLogger(TableUtils.class);
	
	private static final GPUdb connection = 
		GPUConnectionManager.getInstance().getGPUdb();
	
	public static boolean tableExists(
			String tableName) {
		try {
			HasTableRequest request = new HasTableRequest(tableName, null);
			return connection.hasTable(request).getTableExists();
		} catch (GPUdbException e) {
			throw new KineticaException(e);
		}
	}

	public static void createTable(Table table) {
		try {
			logger.info("Creating  table " + table.getName() + " with typeId " + table.getTypeId());
			Map<String, String> options = GPUdb.options(
				CreateTableRequest.Options.NO_ERROR_IF_EXISTS, CreateTableRequest.Options.TRUE);

			if (table.getCollectionName() != null) {
				options.put(CreateTableRequest.Options.COLLECTION_NAME, table.getCollectionName());
			}
			connection.createTable(table.getName(), table.getTypeId(), options);
		} catch (GPUdbException e) {
			throw new KineticaException(e);
		}
	}
}
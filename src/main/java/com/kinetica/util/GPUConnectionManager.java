package com.kinetica.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gpudb.GPUdb;
import com.gpudb.GPUdbBase;

public class GPUConnectionManager {

	private static GPUConnectionManager instance = null;
	private static GPUdb gpudb = null;
	private final static Logger logger = 
			LoggerFactory.getLogger(GPUConnectionManager.class);

	private GPUConnectionManager() {
		logger.info("Connection to Kinetica at: " + KineticaConfiguration.GPUDB_CONNECT_STRING);
		if (gpudb == null) {
			try {
				GPUdbBase.Options opts = getGPUDBOptions();
				gpudb = new GPUdb(KineticaConfiguration.GPUDB_CONNECT_STRING, opts);
			} catch (Exception e) {
				throw new KineticaException(e);
			}
		}
	}

	private GPUdbBase.Options getGPUDBOptions() {
		GPUdbBase.Options opts = new GPUdb.Options();
		boolean enableAuthentication = new Boolean(
			KineticaConfiguration.ENABLE_AUTHENTICATION).booleanValue();
		if (enableAuthentication) {
			opts.setUsername(KineticaConfiguration.USERNAME.trim());
			opts.setPassword(KineticaConfiguration.PASSWORD.trim());					
		}
		return opts;
	}
	
	public static GPUConnectionManager getInstance() {
		if (instance == null) {
			instance = new GPUConnectionManager();
		}
		return instance;
	}
	
	public GPUdb getGPUdb() {
		if (gpudb == null || instance == null)
			throw new KineticaException("Please call getInstance first.");
		return gpudb;
	}
}
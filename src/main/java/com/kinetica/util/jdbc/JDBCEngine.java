package com.kinetica.util.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kinetica.util.KineticaConfiguration;
import com.kinetica.util.KineticaException;

public class JDBCEngine {

	private static JDBCEngine instance = null;
	private Connection conn = null;
	private Statement stmt = null;

	public final static Logger log = 
		LoggerFactory.getLogger(JDBCEngine.class);

	public static JDBCEngine getInstance() {
		if (instance == null) {
			instance = new JDBCEngine();
		}
		return instance;
	}
	
	//Private constructor to avoid instantiation
	private JDBCEngine() {
	}

	public void init() {
		JDBCConnectionManager mgr = JDBCConnectionManager.getInstance();
		conn = mgr.getConnection(KineticaConfiguration.JDBC_CONNECTION_URL);		
	}

	public ResultSet execute(String queryString) {
		try {
			if (conn == null) {
				throw new KineticaException("Please call init first.");
			}
			stmt = conn.createStatement();
			log.info("Running query: " + queryString);
			stmt.execute(queryString);			
			
			return stmt.getResultSet();
		} catch (Exception e) {
			throw new KineticaException(e);
		}
	}

	public void close() {
		try {
			if (stmt != null && !stmt.isClosed()) {
				stmt.close();
			}
			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
		} catch (Exception e) {
			throw new KineticaException(e);
		}
	}	
}

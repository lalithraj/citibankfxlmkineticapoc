package com.kinetica.util.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kinetica.util.KineticaConfiguration;
import com.kinetica.util.KineticaException;

public class JDBCConnectionManager {

	private static JDBCConnectionManager instance = null;
	private final static Logger logger = 
		LoggerFactory.getLogger(JDBCConnectionManager.class);
	private static Connection conn = null;

	private JDBCConnectionManager() {
	}
	
	public static JDBCConnectionManager getInstance() {
		if (instance == null) {
			instance = new JDBCConnectionManager();
		}
		return instance;
	}
	
	public Connection getConnection(String url) {
		if (instance == null)
			throw new KineticaException("Please call getInstance first.");
		logger.info("Connection to Kinetica at: " + KineticaConfiguration.JDBC_CONNECTION_URL);
		try {
			if (conn == null || conn.isClosed()) {
				conn = getConnection("com.simba.client.core.jdbc4.SCJDBC4Driver", url);
			}
		} catch (Exception e) {
			throw new KineticaException("Cannot connect: ", e);
		}
		return conn;
	}
	
	private Connection getConnection(String driverClass, String url) {
		try {
			Class.forName(driverClass);
			Properties p = new Properties();
			p.setProperty("UID", "");
			p.setProperty("PWD", "");
			return DriverManager.getConnection(url, p);
		} catch (Exception e) {
			throw new KineticaException("Cannot connect: ", e);
		}
	}
	
	public void closeConnection() {
		try {
			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
		} catch (Exception e) {
			throw new KineticaException(e);
		}
	}

}
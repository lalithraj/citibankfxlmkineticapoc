package com.kineticatests;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kinetica.query.FXTradeManager;
import com.kinetica.query.domain.BookCurrencyPair;
import com.kinetica.query.domain.Table;
import com.kinetica.query.domain.output.PNL;
import com.kinetica.query.domain.output.PNL_JSON;
import com.kinetica.util.KineticaConfiguration;
import com.kinetica.util.KineticaException;
import com.kinetica.util.KineticaPropertyReader;
import com.kinetica.util.TableUtils;
import com.kinetica.util.TypeUtils;

public class Main {

	public final static Logger log = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) {
		init(args);

		String JDBC_OR_GPUDBFILTER = KineticaConfiguration.JDBC_OR_GPUDBFILTER;
		
		while (true) {
			if (JDBC_OR_GPUDBFILTER.equalsIgnoreCase("JDBC")) {
				FXTradeManager tradeManager = new FXTradeManager();
				List<PNL> pnlList = tradeManager.getPNLListUsingJDBCQuery();
				tradeManager.persistPNLList(pnlList);
				tradeManager.persistPNLListAsJSON(pnlList);
				log.info("Complete ...Waiting for 25 seconds ...");			
				waiting(25*1000);			
			} else if (JDBC_OR_GPUDBFILTER.equalsIgnoreCase("GPUDBFILTER")) {
				FXTradeManager tradeManager = new FXTradeManager();
				List<BookCurrencyPair> bcPairs = tradeManager.
					determineFXTradesGroupByBookCurrencyPair();
				tradeManager.persistbcPairs(bcPairs);
				log.info("Completed entering records in Book Currency pair");
				List<PNL> pnlList = tradeManager.getPNLList(bcPairs);
				tradeManager.persistPNLList(pnlList);
				tradeManager.persistPNLListAsJSON(pnlList);
				log.info("Completed processing PNL.");			
				log.info("Complete ...Waiting for 25 seconds ...");			
				waiting(25*1000);
			}
		}
	}

	private static void init(String[] args) {
		String propertyFile = args.length == 0 ? 
			"scripts/properties/fxTrades.properties": args[0];
		KineticaPropertyReader.init(propertyFile);
		KineticaPropertyReader.validateProperties();
		
		createTableIfNotExists(KineticaConfiguration.COLLECTION_NAME, 
			"BOOK_CURRENCY_PAIR", BookCurrencyPair.class);
		createTableIfNotExists(KineticaConfiguration.COLLECTION_NAME, "PNL", PNL.class);
		createTableIfNotExists(KineticaConfiguration.COLLECTION_NAME, "PNL_JSON", PNL_JSON.class);
	}

	@SuppressWarnings("rawtypes")
	private static void createTableIfNotExists(
			String collectionName, String tableName, Class _class) {
		if (!TableUtils.tableExists(tableName)) {
			com.gpudb.Type type = TypeUtils.getType(_class);
			String typeId = TypeUtils.getTypeId(type);
			
			Table table = new Table(type, typeId);
			table.setCollectionName(collectionName);
			table.setName(tableName);
			TableUtils.createTable(table);
		}
	}

	private static void waiting(int i) {
		try {
			Thread.sleep(25*1000);
		} catch (InterruptedException e) {
			throw new KineticaException(e);
		}		
	}
}
Move the Simba driver to maven
mvn install:install-file -Dfile=SimbaJDBCClient4.jar -DgroupId=com.kinetica -DartifactId=SimbaJDBCClient -Dversion=4.0 -Dpackaging=jar

Now update scripts/properties/fxProperties.properties
And then go to scripts/run directory and ./run.sh